<?php
/**
 * Field handler to present a link to delete a node.
 *
 * @ingroup views_field_handlers
 */
class archive_node_views_handler_field_node_form_actions extends views_handler_field_node_link {
  function render($values){
      $args=array("nid"=>$values->nid);
      $html=drupal_render(drupal_get_form('archive_node_submit_driven_ajax_'.$values->nid, $args));
      return $html;
  }
}

