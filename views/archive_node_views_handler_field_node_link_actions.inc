<?php
/**
 * Field handler to present a link to delete a node.
 *
 * @ingroup views_field_handlers
 */
class archive_node_views_handler_field_node_link_actions extends views_handler_field_node_link {

  /**
   * Renders the link.
   */
  function render_link($node, $values) {
    // Ensure user has access to delete this node.
//     if (!node_access('delete', $node)) {
//       return;
//     }
// 
//     $this->options['alter']['make_link'] = TRUE;
//     $this->options['alter']['path'] = "node/$node->nid/delete";
//     $this->options['alter']['query'] = drupal_get_destination();
// 
//     $text = !empty($this->options['text']) ? $this->options['text'] : t('Arxiva');
      $args=array();
      $html=drupal_render(drupal_get_form('archive_node_submit_driven_ajax', $args));
    return $html.$text;
  }

}

